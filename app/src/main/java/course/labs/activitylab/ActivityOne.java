package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		private int onCreateCount = 0;
		private int onStartCount = 0;
		private int onResumeCount = 0;
		private int onPauseCount = 0;
		private int onStopCount = 0;
		private int onDestroyCount = 0;
		private int onRestartCount = 0;

		// Views handles
		private TextView create, start, resume, pause, stop, destroy, restart;

		//TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
		//TODO:  increment the variables' values when their corresponding lifecycle methods get called.
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);

			// Instantiate the TextViews
			create = (TextView) findViewById(R.id.create);
			start = (TextView) findViewById(R.id.start);
			resume = (TextView) findViewById(R.id.resume);
			pause = (TextView) findViewById(R.id.pause);
			stop = (TextView) findViewById(R.id.stop);
			destroy = (TextView) findViewById(R.id.destroy);
			restart = (TextView) findViewById(R.id.restart);

			//Log cat print out
			Log.i(TAG, "onCreate called");
			onCreateCount++;
			String updatedText = getResources().getString(R.string.onCreate) + " " + onCreateCount;
			Log.i(TAG, "New value for TextView: " + updatedText);
			create.setText(updatedText);
			//TODO: update the appropriate count variable & update the view
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides
		@Override
		public void onStart(){
			super.onStart();

			//Log cat print out
			Log.i(TAG, "onStart called");
			onStartCount++;
			String updatedText = getResources().getString(R.string.onStart) + " " + onStartCount;
			start.setText(updatedText);
			//TODO:  update the appropriate count variable & update the view
		}

		@Override
		public void onResume() {
			super.onResume();

			Log.i(TAG, "onResume called");
			onRestartCount++;
			String updatedText = getResources().getString(R.string.onResume) + " " + onResumeCount;
			resume.setText(updatedText);
		}

		@Override
		public void onPause() {
			super.onPause();

			Log.i(TAG, "onPause called");
			onPauseCount++;
			String updatedText = getResources().getString(R.string.onPause) + " " + onPauseCount;
			pause.setText(updatedText);
		}

		@Override
		public void onDestroy() {
			super.onDestroy();

			Log.i(TAG, "onDestroy called");
			onDestroyCount++;
			String updatedText = getResources().getString(R.string.onDestroy) + " " + onDestroyCount;
			destroy.setText(updatedText);
		}

		@Override
		public void onRestart() {
			super.onRestart();

			Log.i(TAG, "onRestart called");
			onRestartCount++;
			String updatedText = getResources().getString(R.string.onRestart) + " " + onRestartCount;
			restart.setText(updatedText);
		}

	    // Note:  if you want to use a resource as a string you must do the following
	    //  getResources().getString(R.string.stringname)   returns a String.

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			//TODO:  save state information with a collection of key-value pairs & save all  count variables
		}


		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
		

}
